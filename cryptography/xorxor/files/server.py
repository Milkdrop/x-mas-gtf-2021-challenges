import os

flag = b"X-MAS{normal-crypto-only-flips-half-of-the-plaintext-bits}"

def encrypt(a, key):
	enc = []
	for i, c in enumerate(a):
		enc.append(c ^ key[i % len(key)])
	return bytes(enc)

def generateKey():
	global flag
	return os.urandom(len(flag))

key = generateKey()
text = key + encrypt(flag, key)
text = encrypt(text, b'\xff')

print("Hello! This is the encrypted message:")
print(text)
print("Bye!")
