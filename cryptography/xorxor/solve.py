from pwn import *

context.log_level = "CRITICAL"
r = remote('localhost', 1000)

r.recvuntil(b"message:\n")
enc = r.recvuntil(b"\nBye!").split(b"\n")[0]

enc = eval(enc)

# decrypt
enc = xor(enc, 0xff)
flag = xor(enc[:len(enc) // 2], enc[len(enc) // 2:])

print(flag.decode())
